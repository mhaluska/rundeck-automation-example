# Example Ops automation jobs for Rundeck

## Prerequisites

First follow setup guide for Zabbix, Rundeck and OTRS here: [zabbix-rundeck-otrs](https://gitlab.com/mhaluska/zabbix-rundeck-otrs)

All Rundeck key storage paths in jobs are set to keys/automation-example/\*, if you change the key path, then you must also update path in jobs.

Create following keys:
- keys/automation-example/sshkey (key type: Private key, used for ssh access to servers)
- keys/automation-example/zabbixpw (key type: Password, user password with API write access to Zabbix server)
- keys/automation-example/otrspw (key type: Password, customer user password for OTRS API access, use urlencoded value if password contains special characters)
- keys/automation-example/rdtoken (key type: Password, Rundeck token with rights to view execution status)

Create new Rundeck project: automation-example

## Import jobs

Use **Preserve UUIDs** for import, otherwise you need to take care for reference jobs update.

Import jobs in following order (I'm not sure if it really matters, not tested):
1. Internal/Internal-EventStatus-OK.xml
2. Internal/Internal-EventStatus-PROBLEM.xml
3. Internal/Internal-Update-Ticket.xml
4. Internal/Internal-Job-NotFound.xml
5. FileExist/Automation-FileExist-Example.xml

## Update jobs options

Without those steps, your automation will not work!

Update options in following jobs, this step is required:
- Automation-FileExist-Example: no options specified (they're passed by API call)
- Internal-EventStatus-OK: `otrsurl`, `otrsusr`, `rdurl`, `zabbixurl`, `zabbixusr`
- Internal-EventStatus-PROBLEM: `otrsurl`, `otrsusr`, `zabbixurl`, `zabbixusr`
- Internal-Job-NotFound: no update required, feel free to update option `update` with own message
- Internal-Update-Ticket: `otrsurl`, `otrsusr`

I had problem after import, `Import options?` for reference job has been lost. So check following jobs and ensure `Import options?` is enabled.
- Automation-FileExist-Example for reference job `Internal-EventStatus-PROBLEM`
- Internal-Job-NotFound for reference job `Internal-EventStatus-PROBLEM`